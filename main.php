<?php
    if(isset($_GET['x']) && isset($_GET['y']))
    {
        $x = $_GET['x'];
        $y = $_GET['y'];
        if(!$x || !$y){
            echo json_encode("enter numbers");
        }
        elseif($x < -100000000000 || $x > 100000000000 || $y < -100000000000 || $y > 100000000000)
        {
            echo json_encode("Enter numbers within the range");
        }
        else{
            $result['Addition']= $x + $y;
            $result['Subtraction']= $x - $y;
            $result['Multiplication']= $x * $y;
            $result['Division']= $x / $y;
            foreach($result as $key=>$r){
                echo json_encode($key . " = " . $r);
            }
        }
    }
    else{
        echo json_encode("No Data has been entered");
    }
    
    
?>